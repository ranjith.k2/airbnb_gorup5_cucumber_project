package Stepdefeniations;

import static org.testng.Assert.assertEquals;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageobject_model.Login;
import pageobject_model.SearchHotel_page;
import pageobject_model.payement_page;

public class Stepdefeniations {
	static WebDriver driver;

	@Given("user navigate Aribnb applications")
	public void user_navigate_aribnb_applications() {
		driver = new ChromeDriver();
		driver.get("https://www.airbnb.co.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

	}

	@Given("user click the login button")
	public void user_click_the_login_button() throws AWTException, InterruptedException {
		Login l = PageFactory.initElements(driver, Login.class);
		l.clicklog();
		Robot r = new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		l.clickcontinue();

	}

	@Given("user login into the applications with {string} and {string}")
	public void user_login_into_the_applications_with_and(String string, String string2) {
		Login l1 = PageFactory.initElements(driver, Login.class);
		l1.sendemail(string);
		l1.clickeailbutton();
		l1.sendpass(string2);

	}

	@When("user click the login buution")
	public void user_click_the_login_buution() throws InterruptedException {
		Login l2 = PageFactory.initElements(driver, Login.class);
		l2.clickpassbutton();
		Thread.sleep(3000);

	}

	@When("user search holtel in search display page")
	public void user_search_holtel_in_search_display_page() throws InterruptedException {
		SearchHotel_page s = PageFactory.initElements(driver, SearchHotel_page.class);
		s.Filterhotel();

	}

	@When("click holte page button")
	public void click_holte_page_button() {
		SearchHotel_page s1 = PageFactory.initElements(driver, SearchHotel_page.class);

		s1.clickhotel();
		String windowHandle = driver.getWindowHandle();
		System.out.println(windowHandle);

		Set<String> windowHandles = driver.getWindowHandles();
		System.out.println(windowHandles);

		List<String> ref = new ArrayList<String>(windowHandles);
		driver.switchTo().window(ref.get(1));

	}

	@When("choice the payment options")
	public void choice_the_payment_options() throws InterruptedException, AWTException {
		payement_page p = PageFactory.initElements(driver, payement_page.class);

		Thread.sleep(3000);
		p.Resverbutton();
		Thread.sleep(3000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
		p.clickcard();

		Robot r1 = new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);
		r1.keyRelease(KeyEvent.VK_DOWN);
		Thread.sleep(2000);

		r1.keyPress(KeyEvent.VK_ENTER);
		r1.keyRelease(KeyEvent.VK_ENTER);

	}

	@When("enter hotel discriptions")
	public void enter_hotel_discriptions(io.cucumber.datatable.DataTable a) throws InterruptedException {
		payement_page p1 = PageFactory.initElements(driver, payement_page.class);
		List<String> b = a.asList();
		p1.sendfedback(b.get(0));
		Thread.sleep(2000);

		p1.resverclick();

	}

	@Then("the vallid payment page")
	public void the_vallid_payment_page() {
		String currentUrl = driver.getCurrentUrl();
		System.out.println(currentUrl);
//		assertEquals(currentUrl, "https://www.airbnb.co.in/");
//	}
		if (currentUrl.contains("https://www.airbnb.co")) {

			System.out.println("contions paass");
		} else {
			System.out.println("contions fail");

		}
	}
}
