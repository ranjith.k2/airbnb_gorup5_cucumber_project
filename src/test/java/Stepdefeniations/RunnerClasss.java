package Stepdefeniations;


import org.junit.runner.RunWith;

import cucumber.api.testng.AbstractTestNGCucumberTests;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
@RunWith(Cucumber.class)

@CucumberOptions(features = {
		"/home/ranjith/eclipse-workspace/Aribnb_Cucuvmber_framework/src/test/resources/Airbnb/airbnb.feature" }, 
          dryRun = false,
          snippets = SnippetType.CAMELCASE, 
           monochrome = true,
          glue = "Stepdefeniations"

)

public class RunnerClasss extends AbstractTestNGCucumberTests {

}
